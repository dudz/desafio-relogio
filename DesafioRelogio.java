import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.SecureRandom;
import java.util.BitSet;

import javax.imageio.ImageIO;

public class DesafioRelogio {

	private static final int DPI = 1200;
	private static final double CIRCUNFERENCIA_MOSTRADOR = 600;
	private static final double RAIO_MOSTRADOR = CIRCUNFERENCIA_MOSTRADOR / Math.PI / 2.0;
	private static final double LARGURA_MOSTRADOR = 0.5;
	private static final double COMPRIMENTO_RISCO = 3.0;
	private static final double LARGURA_RISCO = 0.25;
	private static final double DIST_MIN_RISCO = RAIO_MOSTRADOR - 1 - COMPRIMENTO_RISCO;
	private static final double RAIO_BOLINHAS = COMPRIMENTO_RISCO / 2.0;
	private static final double DIST_BOLINHAS = RAIO_MOSTRADOR - 1 - RAIO_BOLINHAS;
	private static final double TAM_PONTEIRO_MIN = RAIO_MOSTRADOR - 1 - COMPRIMENTO_RISCO - 1;
	private static final double TAM_PONTEIRO_SEC = TAM_PONTEIRO_MIN;
	private static final double TAM_PONTEIRO_HORA = 0.75 * TAM_PONTEIRO_MIN;
	private static final double TAM_PONTEIRO_DESAFIO = TAM_PONTEIRO_MIN;
	private static final double RAIO_CENTRO_PONTEIRO_MIN = 3.5;
	private static final double RAIO_CENTRO_PONTEIRO_HORA = RAIO_CENTRO_PONTEIRO_MIN;
	private static final double RAIO_CENTRO_PONTEIRO_SEC = RAIO_CENTRO_PONTEIRO_MIN / 2.0;
	private static final double RAIO_CENTRO_PONTEIRO_DESAFIO = RAIO_CENTRO_PONTEIRO_MIN;
	private static final double RAIO_MARCADORES_PRECISAO = LARGURA_RISCO / 2.0;
	private static final double DIST_MARCADORES_PRECISAO = (TAM_PONTEIRO_DESAFIO + DIST_MIN_RISCO) / 2.0;
	//
	private static final double MM_POR_POL = 25.4;
	private static final double DPIMM = DPI / MM_POR_POL;
	private static final double TAM_PAPEL = RAIO_MOSTRADOR * 2.0 + LARGURA_MOSTRADOR + 2.0; // 1mm a mais que a borda do mostrador.
	// dimensao impar para ter centro bem definido
	private static final int DIM = (int)Math.floor(TAM_PAPEL*DPIMM) + (1-((int)Math.floor(TAM_PAPEL*DPIMM)%2));
	private static final int BI_RGB = 0; // Sem compressao BMP

	private static BitSet[] tela;
    static {
        tela = new BitSet[DIM];
        for (int i = 0; i < DIM; i++) {
            tela[i] = new BitSet(DIM);
        }
    }

    public static void main(String[] args) {
    	double horario;
    	int seed;
    	if (args.length > 0 && args[0].matches("^[0-9]{1,9}$")) {
    		seed = Integer.parseInt(args[0]);
    	}
    	else {
    		seed = new SecureRandom().nextInt(1000000000);
    	}
    	horario = mersenneTwisterGetDouble(seed);
    	//horario = 0.986938800653185; // 11:50:35.7561882176, corresponde a seed = 376138343
    	criaDesafio(horario, "desafio_" + seed);
    	criaResposta(horario, "resposta_" + seed);
    	System.out.println("Desafio gerado.");
    }

    // Simula execução do algoritmo Mersenne Twister, para gerar número pseudo-aleatório que seja o mesmo
    // da versão em C.
    private static double mersenneTwisterGetDouble(int seed) {
    	int n = 624;
    	int m = 397;
    	long matrix_a = 0x9908b0dfL;   /* constant vector a */
    	long upper_mask = 0x80000000L; /* most significant w-r bits */
    	long lower_mask = 0x7fffffffL; /* least significant r bits */

    	long mt[] = new long[n]; /* the array for the state vector  */
    	int mti = n + 1; /* mti==N+1 means mt[N] is not initialized */

    	mt[0]= seed;
        for (mti = 1; mti < n; mti++) {
            mt[mti] =  (1812433253L * (mt[mti-1] ^ (mt[mti-1] >>> 30)) + mti);
            mt[mti] &= 0xffffffffL;
        }
        //------
        long y;
        long mag01[]= new long[]{0, matrix_a}; /* mag01[x] = x * MATRIX_A  for x=0,1 */

        int kk;

        for (kk = 0; kk < n - m; kk++) {
            y = (mt[kk] & upper_mask) | (mt[kk + 1] & lower_mask);
            mt[kk] = mt[kk + m] ^ (y >>> 1) ^ mag01[(int)(y & 1)];
        }
        for (; kk < n - 1; kk++) {
            y = (mt[kk] & upper_mask) | (mt[kk + 1] & lower_mask);
            mt[kk] = mt[kk + (m - n)] ^ (y >>> 1) ^ mag01[(int)(y & 1)];
        }
        y = (mt[n - 1] & upper_mask) | (mt[0] & lower_mask);
        mt[n - 1] = mt[m - 1] ^ (y >>> 1) ^ mag01[(int)(y & 1)];

        mti = 0;

        y = mt[mti++];
        /* Tempering */
        y ^= (y >>> 11);
        y ^= (y << 7) & 0x9d2c5680L;
        y ^= (y << 15) & 0xefc60000L;
        y ^= (y >>> 18);
        y &= 0xffffffffL;

        long a = y >>> 5;

        y = mt[mti++];
        /* Tempering */
        y ^= (y >>> 11);
        y ^= (y << 7) & 0x9d2c5680L;
        y ^= (y << 15) & 0xefc60000L;
        y ^= (y >>> 18);
        y &= 0xffffffffL;

        long b = y >>> 6;

        return (a * 67108864.0 + b) / 9007199254740992.0;
	}

	private static void criaDesafio(double horario, String nomeArqBase) {
    	limpaTela();
        desenhaMoldeRelogio(true);
        desenhaPonteirosDesafio(horario);
        geraImagem(nomeArqBase.concat(".png"));
    }

	private static void criaResposta(double horario, String nomeArqBase) {
    	limpaTela();
        desenhaMoldeRelogio(false);
        desenhaPonteirosResposta(horario);
        geraImagem(nomeArqBase.concat(".png"));
    }

	private static void desenhaPonteirosResposta(double horario) {
        double hora, min, sec;

        hora = horario * 360.0;
        min = 12.0 * (hora - (30.0 * Math.floor(hora / 30.0)));
        sec = 60.0 * (min - (6.0 * Math.floor(min / 6.0)));

        desenhaPonteiroHora(hora);
        desenhaPonteiroMin(min);
        desenhaPonteiroSec(sec);
	}

	private static void desenhaPonteiroHora(double angGraus) {
        double ang;
        double dist;
        double tamPontoD;
        int tamPonto;
        double maxDist;

        ang = Math.toRadians(normalizaAngulo(360.0 - angGraus + 90.0));

        maxDist = TAM_PONTEIRO_HORA * DPIMM;

        for (dist = 0.0; dist <= maxDist; dist += 0.25) {
            tamPontoD = RAIO_CENTRO_PONTEIRO_HORA * (1.0 - (dist / maxDist)) * DPIMM;
            tamPonto = (int) Math.round(tamPontoD);
            pset((DIM/2) + Math.cos(ang) * dist, (DIM/2) + Math.sin(ang) * dist, tamPonto);
        }
    }

	private static void desenhaPonteiroMin(double angGraus) {
        double ang;
        double dist;
        double tamPontoD;
        int tamPonto;
        double maxDist;

        ang = Math.toRadians(normalizaAngulo(360.0 - angGraus + 90.0));

        maxDist = TAM_PONTEIRO_MIN * DPIMM;

        for (dist = 0.0; dist <= maxDist; dist += 0.25) {
            tamPontoD = RAIO_CENTRO_PONTEIRO_MIN * (1.0 - (dist / maxDist)) * DPIMM;
            tamPonto = (int) Math.round(tamPontoD);
            pset((DIM/2) + Math.cos(ang) * dist, (DIM/2) + Math.sin(ang) * dist, tamPonto);
        }
    }

	private static void desenhaPonteiroSec(double angGraus) {
        double ang;
        double dist;
        double tamPontoD;
        int tamPonto;
        double maxDist;

        ang = Math.toRadians(normalizaAngulo(360.0 - angGraus + 90.0));

        maxDist = TAM_PONTEIRO_SEC * DPIMM;

        for (dist = 0.0; dist <= maxDist; dist += 0.25) {
            tamPontoD = RAIO_CENTRO_PONTEIRO_SEC * (1.0 - (dist / maxDist)) * DPIMM;
            tamPonto = (int) Math.round(tamPontoD);
            pset((DIM/2) + Math.cos(ang) * dist, (DIM/2) + Math.sin(ang) * dist, tamPonto);
        }
    }

	private static void desenhaPonteirosDesafio(double horario) {

        double hora, min, sec;
        int i;

        hora = horario * 360.0;

        min = 12.0 * (hora - (30.0 * Math.floor(hora / 30.0)));
        sec = 60.0 * (min - (6.0 * Math.floor(min / 6.0)));

        double[] angs = new double[] {hora, min, sec};

        double minDist = Double.MAX_VALUE, dist, desloc = 0.0;
        for (i = 0; i < 3; i++) {
            dist = angs[i] - (30.0 * Math.floor(angs[i] / 30.0));
            if (dist < minDist) {
                desloc = angs[i] - dist;
                minDist = dist;
            }
        }
        for (i = 0; i < 3; i++) {
            desenhaPonteiroDesafio(angs[i] - desloc);
        }
    }

	private static void desenhaPonteiroDesafio(double angGraus) {
        double ang;
        double dist;
        double tamPontoD;
        int tamPonto;
        double maxDist;

        ang = Math.toRadians(normalizaAngulo(360.0 - angGraus + 90.0));

        maxDist = TAM_PONTEIRO_DESAFIO * DPIMM;

        for (dist = 0.0; dist <= maxDist; dist += 0.25) {
            tamPontoD = RAIO_CENTRO_PONTEIRO_DESAFIO * (1.0 - (dist / maxDist)) * DPIMM;
            tamPonto = (int) Math.round(tamPontoD);
            pset((DIM/2) + Math.cos(ang) * dist, (DIM/2) + Math.sin(ang) * dist, tamPonto);
        }
    }

	private static double normalizaAngulo(double x) {
        while (x < 0.0) x += 360.0;
        while (x >= 360.0) x -= 360.0;
        return x;
    }

	private static void geraImagem(String nomeArq) {
    	int i, j;
    	int qtdBitsPad;
        BufferedOutputStream out = null;
        int byteVal;
        int contBits = 0;
        String nomeTmp = nomeArq + ".bmp";

        qtdBitsPad = (32 - (DIM % 32)) % 32;
        try {
			out = new BufferedOutputStream(new FileOutputStream(nomeTmp));
        /*// Versão PBM
        out.println("P1");
        out.println(""+DIM + " " + DIM);
        for (i = DIM - 1; i >= 0; i--) {
            for (j = 0; j < DIM; j++) {
                if (tela[i].get(j)) out.print("1 ");
                else out.print("0 ");
            }
            out.println();
            out.flush();
        }
        //*/
			out.write(cabecalhoBmp());
			out.write(paletaBmp());

			for (i = 0; i < DIM; i++) { // bmp grava de baixo para cima
				byteVal = 0;
				contBits = 0;
				for (j = 0; j < DIM; j++) {
					byteVal = (byteVal << 1) | (tela[i].get(j) ? 0 : 1);
					if (++contBits == 8) {
						contBits = 0;
						out.write(byteVal);
						byteVal = 0;
					}
				}
				for (j = 0; j < qtdBitsPad; j++) {
					byteVal <<= 1;
					if (++contBits == 8) {
						contBits = 0;
						out.write(byteVal);
						byteVal = 0;
					}
				}
			}
			out.close();

			// Conversão para PNG

		    BufferedImage inputImage = null;
		    File inputFile = new File(nomeTmp);
		    inputImage = ImageIO.read(inputFile); //read bmp into input_image object
		    File outputFile = new File(nomeArq); //create new outputfile object
		    ImageIO.write(inputImage, "PNG", outputFile); //write PNG output to file
		    inputFile.delete();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Erro ao criar " + nomeArq);
			System.exit(1);
		}
    }

	private static byte[] paletaBmp() {
    	int intVal;
		ByteBuffer buf = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);

		intVal = 0;
		buf.putInt(intVal); // preto
		intVal = 0xffffff;
		buf.putInt(intVal); // branco

		return buf.array();
    }

	private static byte[] cabecalhoBmp() {
    	int intVal;
    	short shortVal;
		ByteBuffer buf = ByteBuffer.allocate(54).order(ByteOrder.LITTLE_ENDIAN);
		int qtdBitsPad = (32 - (DIM % 32)) % 32;
		// Bitmap file header
		buf.put("BM".getBytes()); // assinatura
		intVal = 14 + 40 + 8 + (DIM * ((DIM + qtdBitsPad) / 8)); // tamanho bmp
		buf.putInt(intVal);
		intVal = 0;
		buf.putInt(intVal); // dois reservados de dois bytes
		intVal = 14 + 40 + 8; // deslocamento até os pixels
		buf.putInt(intVal);

		// DIB header (bitmap information header): BITMAPINFOHEADER
		intVal = 40; // tamanho header
		buf.putInt(intVal);
		intVal = DIM;
		buf.putInt(intVal); // largura
		buf.putInt(intVal); // altura
		shortVal = 1;
		buf.putShort(shortVal); // planos
		shortVal = 1;
		buf.putShort(shortVal); // bits por pixel
		intVal = BI_RGB;
		buf.putInt(intVal); // tipo compressão (sem compressão)
		intVal = (DIM * ((DIM + qtdBitsPad) / 8));
		buf.putInt(intVal); // tamanho dados bmp
		intVal = (int) Math.round((DPI / MM_POR_POL) * 1000.0); // resolução
		buf.putInt(intVal); // resolução horizontal
		buf.putInt(intVal); // resolução vertical
		intVal = 2; // cores na paleta
		buf.putInt(intVal);
		intVal = 2; // cores importantes
		buf.putInt(intVal);

		return buf.array();
	}

	private static void pset (double x, double y, int larguraExtra) {
        int i, j;
        int iIni, jIni;
        iIni = (int) Math.round(y);
        jIni = (int) Math.round(x);
        for (i = 0; i <= larguraExtra; i++) {
            for (j = 0; j <= larguraExtra; j++) {
                if (Math.sqrt(Math.pow(j, 2.0) + Math.pow(i, 2.0)) > larguraExtra) continue;
                pset(jIni+j,iIni+i);
                pset(jIni-j,iIni+i);
                pset(jIni+j,iIni-i);
                pset(jIni-j,iIni-i);
            }
        }
    }

	private static void pset(int j, int i) {
        tela[i].set(j);
    }

	private static void desenhaMoldeRelogio (boolean desenharMarcadoresPrecisos) {
        desenhaCircunferenciaExterna();
        desenhaMarcadoresCirculares();
        desenhaMarcadoresRetos();
        if (desenharMarcadoresPrecisos) {
            desenhaMarcadoresPrecisos();
        }
    }

	private static void desenhaMarcadoresPrecisos() {
        double ang;
        double x, y;
        double distPixels = DIST_MARCADORES_PRECISAO * DPIMM;
        int pixelsRaioBolinha = (int)Math.ceil(((2 * RAIO_MARCADORES_PRECISAO * DPIMM) - 1) / 2.0);
        for (int angGraus = 0; angGraus < 360; angGraus++) {
            ang = Math.toRadians(angGraus);
            x = (DIM/2) + Math.cos(ang) * distPixels;
            y = (DIM/2) + Math.sin(ang) * distPixels;
            pset(x, y, pixelsRaioBolinha);
        }
	}

	private static void desenhaMarcadoresRetos() {
        double ang;
        double tam;
        int pixelsLarguraRisco = (int) Math.ceil((LARGURA_RISCO * DPIMM - 1) / 2.0);
        double distMinRisco = DIST_MIN_RISCO * DPIMM + pixelsLarguraRisco;
        double distMaxRisco = distMinRisco + (COMPRIMENTO_RISCO * DPIMM) - pixelsLarguraRisco;

        for (tam = distMinRisco; tam <= distMaxRisco; tam += 0.25) {
            for (int angGraus = 0; angGraus < 360; angGraus += 6) {
                if (angGraus % 30 == 0) continue;
                ang = Math.toRadians(angGraus);
                pset((DIM/2) + Math.cos(ang) * tam, (DIM/2) + Math.sin(ang) * tam, pixelsLarguraRisco);
            }
        }
    }

	private static void desenhaMarcadoresCirculares() {
        double ang;
        double x, y;
        double distPixels = DIST_BOLINHAS * DPIMM;
        int pixelsRaioBolinha = (int)Math.ceil(((2 * RAIO_BOLINHAS * DPIMM) - 1) / 2.0);
        for (int angGraus = 0; angGraus < 360; angGraus += 30) {
            ang = Math.toRadians(angGraus);
            x = (DIM/2) + Math.cos(ang) * distPixels;
            y = (DIM/2) + Math.sin(ang) * distPixels;
            pset(x, y, pixelsRaioBolinha);
        }
    }

	private static void desenhaCircunferenciaExterna() {
        double ang;
        double x, y;
        double circunferenciaPixels = CIRCUNFERENCIA_MOSTRADOR * DPIMM;
        int pixelsLargura = (int) Math.ceil((LARGURA_MOSTRADOR * DPIMM - 1) / 2.0);
        double max = circunferenciaPixels * 4;
        for (int i = 0; i <= max; i++) {
            ang = Math.toRadians(360.0 * (i / max));
            x = (DIM/2) + (RAIO_MOSTRADOR * DPIMM * Math.cos(ang));
            y = (DIM/2) + (RAIO_MOSTRADOR * DPIMM * Math.sin(ang));
            pset(x, y, pixelsLargura);
        }
    }

	private static void limpaTela() {
		for (BitSet b : tela) {
			b.clear();
		}
	}
}
